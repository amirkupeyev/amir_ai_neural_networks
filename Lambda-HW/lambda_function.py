import tensorflow as tf
import tensorflow.lite as tflite
from PIL import Image 
import numpy as np 
from io import BytesIO
from urllib import request


interpreter = tflite.Interpreter(model_path='dino-vs-dragon-v2.tflite')
interpreter.allocate_tensors()  
input_index = interpreter.get_input_details()[0]['index']
output_index = interpreter.get_output_details()[0]['index'] 

classes = [
    'dino',
    'dragon'
] 

def download_image(url):
        with request.urlopen(url) as resp:
            buffer = resp.read()
        stream = BytesIO(buffer)
        img = Image.open(stream)
        return img

def preprocess_input(x):
        x /= 127.5
        x -= 1.
        return x 


def predict(url):
    img = download_image(url)
    x = np.array(img, dtype='float32')
    X = np.array([x])

    X = preprocess_input(X) 
    interpreter.set_tensor(input_index, X)
    interpreter.invoke()
    preds = interpreter.get_tensor(output_index)
    float_predictions = preds[0].tolist()

    return dict(zip(classes, float_predictions)) 

def lambda_handler(event, context):
    url = event['url']
    result = predict(url)
    return result